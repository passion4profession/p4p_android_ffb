package net.p4p.fastfatburning.workoutwizard;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Property;
import android.view.animation.DecelerateInterpolator;

import net.p4p.fastfatburning.R;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RulerDrawable extends Drawable {
    private static final String TAG = RulerDrawable.class.getName();
    private static final float PIXEL_DISTANCE_BETWEEN_NUMBERS = 70;

    private float initialSeekerPosition;
    private Activity context;
    private Paint paint;
    private float noShiftRadius;
    private float sensitivity;
    private float accelerationCurveAmplifier;
    private float shiftStep;
    private MiddlePoint midPoint;
    private ArrayList<OnValueChangeListener> listeners;

    ScheduledExecutorService scheduler;

    private final Property<RulerDrawable, Float> PROGRESS =
            new Property<RulerDrawable, Float>(Float.class, "PROGRESS") {
                @Override
                public Float get(RulerDrawable d) {
                    return d.getCoordOfNumber(getCurrentMiddleValue());
                }

                @Override
                public void set(RulerDrawable d, Float value) {
                    d.setPositionToNumber(getCurrentMiddleValue(), value);
                }
            };

//    public class Point {
//        private int value;
//        private float coord;
//
//        public Point(int value, float coord) {
//            this.value = value;
//            this.coord = coord;
//        }
//
//        public int getValue() {
//            return value;
//        }
//
//        public void setValue(int value) {
//            this.value = value;
//        }
//
//        public float getCoord() {
//            return coord;
//        }
//
//        public void setCoord(float coord) {
//            this.coord = coord;
//        }
//    }

    public interface OnValueChangeListener {
        void onValueChange(int previous, int current);
    }

    private class MiddlePoint {
        private int pointValue;
        private float initialXCoord;
        private float currentXCoord;
        private float xStep;

        public int getPointValue() {
            return pointValue;
        }

        public void setPointValue(int pointValue) {
            this.pointValue = pointValue;
        }

        public float getInitialXCoord() {
            return initialXCoord;
        }

        public float getCurrentXCoord() {
            return currentXCoord;
        }

        public void setCurrentXCoord(float currentXCoord) {
            this.currentXCoord = currentXCoord;
        }

        public float getxStep() {
            return xStep;
        }

        public void setxStep(float xStep) {
            this.xStep = xStep;
        }

        MiddlePoint(int pointValue, float xStep) {
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            this.initialXCoord = metrics.widthPixels / 2.0f;
            this.currentXCoord = initialXCoord;
            this.pointValue = pointValue;
            this.xStep = xStep;
        }

        public void shift(float shiftValue) {
            float potentialCoord = currentXCoord + shiftValue;
            float diff = initialXCoord - potentialCoord;
            if (diff > (xStep / 2.0)) {
                currentXCoord = potentialCoord;
                while (diff > (xStep / 2.0)) {
                    diff -= xStep;
                    currentXCoord += xStep;
                    pointValue++;
                    valueChanged(pointValue-1, pointValue);
                }
            } else if (diff < -(xStep / 2.0)) {
                currentXCoord = potentialCoord;
                while (diff < -(xStep / 2.0)) {
                    diff += xStep;
                    currentXCoord -= xStep;
                    pointValue--;
                    valueChanged(pointValue+1, pointValue);
                }
            } else {
                currentXCoord = potentialCoord;
            }
        }
    }

    public RulerDrawable(Activity context, int minValue, int maxValue, int currentMiddleValue, float noShiftRadius, float sensitivity, float accelerationCurveAmplifier) {
        scheduler = null;
        this.context = context;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        initialSeekerPosition = (width / 2);

        this.noShiftRadius = initialSeekerPosition * noShiftRadius;

        this.sensitivity = Math.abs(sensitivity);
        this.accelerationCurveAmplifier = accelerationCurveAmplifier;

        midPoint = new MiddlePoint(currentMiddleValue, PIXEL_DISTANCE_BETWEEN_NUMBERS);
        listeners = new ArrayList<>();
        setPaint();
    }

    private void setPaint() {
        paint = new Paint();
        paint.setColor(ContextCompat.getColor(context, R.color.colorWhite));
        paint.setStrokeWidth(5f);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setTextSize(50);
        paint.setAntiAlias(true);
    }

    public void addOnValueChangedListener(OnValueChangeListener listener) {
        listeners.add(listener);
    }
    public void removeOnValueChangedListener(OnValueChangeListener listener) {
        listeners.remove(listener);
    }

    private void valueChanged(int previousValue, int currentValue) {
        for (OnValueChangeListener listener : listeners) {
            listener.onValueChange(previousValue, currentValue);
        }
    }
//    public float getInitialSeekerPosition() {
//        return initialSeekerPosition;
//    }

//    public void setInitialSeekerPosition(float initialSeekerPosition) {
//        this.initialSeekerPosition = initialSeekerPosition;
//    }

//    public float getSeekerPosition() {
//        return seekerPosition;
//    }

    public void setSeekerPosition(float deltaX) {

//        float xCoord = view.getX();
//        view.setX(xCoord + deltaX);

        midPoint.shift(deltaX);
        invalidateSelf();
//        updateRuler();
    }

    public int getCurrentMiddleValue() {
        return midPoint.getPointValue();
    }

//    public Point getSeekerClosestNumber() {
//        float x = midPoint.currentXCoord;
//        float coord = 0;
//        int value = midPoint.pointValue;
//        if (initialSeekerPosition - seekerPosition < 0) {
//            while (x < seekerPosition) {
//                x += midPoint.getxStep();
//                value++;
//            }
//            if (x - midPoint.getxStep() / 2 < seekerPosition) {
//                coord = x;
//            } else {
//                coord = x - midPoint.getxStep();
//                value--;
//            }
//        } else if (initialSeekerPosition - seekerPosition > 0) {
//            while (x > seekerPosition) {
//                x -= midPoint.getxStep();
//                value--;
//            }
//            if (x + midPoint.getxStep() / 2 > seekerPosition) {
//                coord = x;
//            } else {
//                coord = x + midPoint.getxStep();
//                value++;
//            }
//        } else {
//            coord = x;
//        }
//        return new Point(value, coord);
//    }

//    public void updateData(float shiftStep) {
//
//        midPoint.shift(shiftStep);
//        Handler mainHandler = new Handler(context.getMainLooper());
//        Runnable r = new Runnable() {
//            @Override
//            public void run() {
//                invalidateSelf();
//            }
//        };
//        mainHandler.post(r);
//    }

    private float getCoordOfNumber(int number) {
        int value = midPoint.pointValue;
        float coord = midPoint.currentXCoord;
        if (value < number) {
            while (value < number) {
                value++;
                coord += midPoint.xStep;
            }
        } else if (value > number) {
            while (value > number) {
                value--;
                coord -= midPoint.xStep;
            }
        }
        return coord;
    }

    public void setPositionToNumber(int number, float coord) {
        float oldCoord = getCoordOfNumber(number);
        float diff = coord - oldCoord;
        midPoint.shift(diff);
//        invalidateSelf();
        Handler mainHandler = new Handler(context.getMainLooper());
        Runnable r = new Runnable() {
            @Override
            public void run() {
                invalidateSelf();
            }
        };
        mainHandler.post(r);
    }

    public void stopScrolling(float deltaX) {
        if (Math.abs(deltaX) < 10) {
            abruptStop();
        } else {
            easeDown(deltaX);
        }
    }

    public void pinDown() {
        if (scheduler != null) {
            scheduler.shutdown();
            scheduler = null;
        }
    }

    private void abruptStop() {
        float fromValue = getCoordOfNumber(getCurrentMiddleValue());
        float toValue = midPoint.getInitialXCoord();
        setPositionToNumber(getCurrentMiddleValue(), midPoint.getInitialXCoord());
        final Animator anim = ObjectAnimator.ofFloat(this, PROGRESS, fromValue, toValue);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
//                    mIsPlay = !mIsPlay;
            }
        });
        anim.setInterpolator(new DecelerateInterpolator(3));
        anim.setDuration(600);
        anim.start();
    }

    private class SlideToNumber implements Runnable {
        private float totalPath;
        private float currentStep;
        float direction;

        public SlideToNumber(float initialSpeed) {


            int targetNumber = getCurrentMiddleValue() - (int)Math.floor(initialSpeed * 2);
            float targetCoord = getCoordOfNumber(targetNumber);
            totalPath = midPoint.initialXCoord - targetCoord;
            direction = Math.signum(totalPath);
            totalPath = Math.abs(totalPath);
        }

        @Override
        public void run() {
            currentStep = totalPath / 10;

            if (currentStep <= 1) {
                currentStep = 1;
            }

            if (totalPath > 0) {
                if (totalPath >= currentStep) {
                    midPoint.shift(currentStep * direction);
                } else {
                    midPoint.shift(totalPath * direction);
                    totalPath = 0;
                }
                totalPath -= currentStep;
                Handler mainHandler = new Handler(context.getMainLooper());
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        invalidateSelf();
                    }
                };
                mainHandler.post(r);
            } else {
                pinDown();
            }

        }
    }

    private void easeDown(float initialSpeed) {


//        float step = initialSeekerPosition - seekerPosition;
//        if (Math.abs(initialSeekerPosition - seekerPosition) > noShiftRadius) {
//            shiftStep = interpolateStep(step);
        pinDown();
        scheduler =
                Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate
                (new SlideToNumber(initialSpeed), 0, 5, TimeUnit.MILLISECONDS);
    }

    private void updateRuler() {

    }

    private float interpolateStep(float step) {
        float absValue = Math.abs(step) - noShiftRadius;
        float maxValue = initialSeekerPosition - noShiftRadius;
        float ratio = absValue / maxValue;

        shiftStep = (float) Math.pow(ratio, accelerationCurveAmplifier) * Math.signum(step);

//        shiftStep = (Math.abs(step) - noShiftRadius) * Math.signum(step);
        return shiftStep * sensitivity;
    }

    @Override
    public void draw(Canvas canvas) {

        DisplayMetrics metrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float width = metrics.widthPixels;
        float x = midPoint.getCurrentXCoord();
        int value = midPoint.getPointValue();
        int y = 0;
        while (x >= -50) {
            if (value % 5 != 0) {
                y = 200;
            } else {
                y = 230;
                canvas.drawText(String.valueOf(value), x - 15, y + 50, paint);
            }
            canvas.drawLine(x, 80, x, y, paint);

            x -= midPoint.getxStep();
            value--;
        }
        x = midPoint.getCurrentXCoord() + midPoint.getxStep();
        value = midPoint.getPointValue() + 1;

        while (x <= width) {
            if (value % 5 != 0) {
                y = 200;
            } else {
                y = 230;
                canvas.drawText(String.valueOf(value), x - 15, y + 50, paint);
            }
            canvas.drawLine(x, 80, x, y, paint);

            x += midPoint.getxStep();
            value++;
        }
    }

    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        paint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

}