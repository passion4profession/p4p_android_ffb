package net.p4p.fastfatburning.workoutwizard;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import net.p4p.fastfatburning.R;

public class SeekerBackground extends Drawable {

    private Context context;
    private Paint paint;
    private int radius;

    public SeekerBackground(Context context, int radius) {
        BitmapShader shader;
//        shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP,
//                Shader.TileMode.CLAMP);
        this.radius = radius;
        paint = new Paint();
        this.context = context;
        paint.setAntiAlias(true);
//        paint.setShader(shader);
    }

    @Override
    public void draw(Canvas canvas) {
        int height = getBounds().height();
        int width = getBounds().width();
//        RectF rect = new RectF(0.0f, 0.0f, width, height);
//        canvas.drawRoundRect(rect, 30, 30, paint);
        paint.setColor(ContextCompat.getColor(context, R.color.colorWhite));
        canvas.drawCircle(radius, radius, radius, paint);

        int yCoord = radius * 2 - 15;
        int innerRectRadius = 40;
        Path path = new Path();
        path.lineTo(radius - innerRectRadius, yCoord);
        path.lineTo(radius, yCoord + innerRectRadius);
        path.lineTo(radius + innerRectRadius, yCoord);
        path.lineTo(radius - innerRectRadius, yCoord);
        canvas.drawPath(path, paint);
        paint.setColor(ContextCompat.getColor(context, R.color.colorPrimary));
        canvas.drawCircle(radius, radius, radius-20, paint);
    }

    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        paint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

}