package net.p4p.fastfatburning.workoutwizard;

import android.app.Activity;
import android.widget.ImageView;

/**
 * Created by timur on 6/15/16.
 */
public class Seeker implements RulerDrawable.OnValueChangeListener{
    private static final String TAG = Seeker.class.getName();
    private ImageView imageView;
    private Activity context;
    private TextDrawable drawable;
    public Seeker (Activity activity, ImageView imageView, String value) {
        this.context = activity;
        this.imageView = imageView;


        drawable = new TextDrawable(activity, value);
        imageView.setImageDrawable(drawable);

    }

    @Override
    public void onValueChange(int previous, int current) {
        TextDrawable.CurtainDirection direction;
        if (previous > current) {
            direction = TextDrawable.CurtainDirection.RIGHT;
        } else {
            direction = TextDrawable.CurtainDirection.LEFT;
        }
        String text = String.valueOf(current) + String.valueOf(current) + String.valueOf(current) + String.valueOf(current) + String.valueOf(current) + String.valueOf(current);
        drawable.setText(text, direction);
    }
}
