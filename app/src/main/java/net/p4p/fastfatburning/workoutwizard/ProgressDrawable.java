package net.p4p.fastfatburning.workoutwizard;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.Xfermode;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Property;

import net.p4p.fastfatburning.R;

import java.util.ArrayList;

/**
 * Created by timur on 8/8/16.
 */
public class ProgressDrawable extends Drawable {
    private static final float ballNumber = 3f;
    private static final float ballRadius = 20f;
    private static final float ballDistance = 60f;
    private static final float lineThickness = 10f;
    private static final String TAG = ProgressDrawable.class.getName();


    private final Property<ProgressDrawable, Float> progress =
            new Property<ProgressDrawable, Float>(Float.class, "progress") {
                @Override
                public Float get(ProgressDrawable d) {
                    return frontline;
                }

                @Override
                public void set(ProgressDrawable d, Float value) {
                    frontline = value;
                    invalidateSelf();
                }
            };


    Activity context;
    Paint paint;
    private float frontline;

    public ProgressDrawable(Activity context) {
//        Log.e(TAG, "WIDTH: " + getIntrinsicWidth());
        this.context = context;
        setPaint();
        frontline = ballRadius * 2;
    }

    private void setPaint() {
        paint = new Paint();
        paint.setStrokeWidth(5f);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setTextSize(50);
        paint.setAntiAlias(true);
    }

    public void stepForward() {
        Animator animation = ObjectAnimator.ofFloat(this, progress, frontline, frontline + ballDistance);
        animation.setDuration(2000);
        animation.start();
    }

    public void stepBackward() {
        Animator animation = ObjectAnimator.ofFloat(this, progress, frontline, frontline - ballDistance);
        animation.setDuration(2000);
        animation.start();
    }

    @Override
    public void draw(Canvas canvas) {
//        canvas.drawColor(context.getResources().getColor(R.color.Y2));

        drawRings(R.color.G2, canvas);
        drawLayer(frontline - ballRadius * 2, frontline, R.color.colorPrimary, canvas);
        drawLayer(0, frontline - ballRadius * 2, R.color.G2, canvas);
    }

    private void drawRings(int color, Canvas canvas) {
        Bitmap b = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        paint.setColor(ContextCompat.getColor(context, color));
        for (int i = 0; i < ballNumber; i++) {
            c.drawCircle(ballRadius + (ballDistance * i), ballRadius, ballRadius, paint);
        }
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        for (int i = 0; i < ballNumber; i++) {
            c.drawCircle(ballRadius + (ballDistance * i), ballRadius, ballRadius-lineThickness, paint);
        }
        paint.setXfermode(null);
        canvas.drawBitmap(b, 0, 0, paint);
    }

    private void drawLayer(float leftBorder, float rightBorder, int color, Canvas canvas) {
        paint.setColor(ContextCompat.getColor(context, color));
        Bitmap b = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        Path path = new Path();
        path.moveTo(leftBorder, 0);
        path.lineTo(leftBorder, canvas.getHeight());
        path.lineTo(rightBorder, canvas.getHeight());
        path.lineTo(rightBorder, 0);
        c.clipPath(path);
        path = new Path();
        float leftX = ballRadius;
        float rightX = ballRadius + (ballDistance * (ballNumber - 1f));
        float topY = ballRadius - (lineThickness / 2f);
        float bottomY = ballRadius + (lineThickness / 2f);

        path.moveTo(leftX, topY);
        path.lineTo(rightX, topY);
        path.lineTo(rightX, bottomY);
        path.lineTo(leftX, bottomY);
        path.lineTo(leftX, topY);
        c.drawPath(path, paint);
        for (int i = 0; i < ballNumber; i++) {
            c.drawCircle(leftX + (ballDistance * i), ballRadius, ballRadius, paint);
        }
        canvas.drawBitmap(b, 0, 0, paint);
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }
}
