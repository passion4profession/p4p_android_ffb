package net.p4p.fastfatburning.workoutwizard;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Property;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import net.p4p.fastfatburning.R;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by timur on 6/8/16.
 */
public class WorkoutComposerFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = WorkoutComposerFragment.class.getName();
    private static final String RULER_MIDDLE_POINT_KEY = "rullerMiddle";
    private static final String ACTIVE_PAGE = "activePage";

    private MotionController motionController;
    View rulerContainer;
    ImageView rulerImage;
    RulerDrawable ruler;
    ProgressDrawable progressDrawable;
    Seeker seeker;
    ArrayList<Page> pages;
    private int activePage;

    private class Page {
        public static final int BACK = 0;
        public static final int MIDDLE = 1;
        public static final int FRONT = 3;
        private static final int axisShift = 200;
        private final Property<Page, Integer> strafe =
                new Property<Page, Integer>(Integer.class, "strafe") {
                    @Override
                    public Integer get(Page d) {
                        return view.getLeft();
                    }

                    @Override
                    public void set(Page d, Integer value) {
                        view.setLeft(value);
                    }
                };
        private View view;
        private int currentPosition;

        public Page(View view, int position) {
            this.view = view;
            this.currentPosition = position;
            if (position == BACK || position == FRONT) {
                view.setAlpha(0f);
//                view.setVisibility(View.INVISIBLE);
                if (position == BACK) {
                    view.setLeft(-axisShift);
                } else {
                    view.setLeft(axisShift);
                }
            } else {
//                view.setVisibility(View.VISIBLE);
                view.setAlpha(1f);
                view.setLeft(0);
            }
        }

        public void setPosition(int position) {
            final float alphaFrom, alphaTo;
            int strafeFrom, strafeTo;
            if (position == BACK || position == FRONT) {
                alphaFrom = 1.0f;
                alphaTo = 0f;
                strafeFrom = 0;
                if (position == BACK) {
                    strafeTo = -axisShift;
                } else {
                    strafeTo = axisShift;
                }
            } else {
                alphaFrom = 0f;
                alphaTo = 1f;
                strafeTo = 0;
                if (currentPosition == BACK) {
                    strafeFrom = -axisShift;
                } else {
                    strafeFrom = axisShift;
                }
            }

            Animator fade = ObjectAnimator.ofFloat(view, "alpha", alphaFrom, alphaTo);
            fade.setDuration(2000);

            Animator slide = ObjectAnimator.ofInt(Page.this, strafe, strafeFrom, strafeTo);
            slide.setDuration(2000);
//
            AnimatorSet set = new AnimatorSet();
            set.playTogether(fade, slide);
            set.start();
            this.currentPosition = position;
        }
    }

    public int getActivePage() {
        return activePage;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_workout_composer, container, false);
        View view = rootView.findViewById(R.id.seeker);
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = 400;
        view.setLayoutParams(params);
        view.setBackground(new SeekerBackground(getActivity(), 200));

        int initialValue = 0;
        if (savedInstanceState != null) {
            initialValue = savedInstanceState.getInt(RULER_MIDDLE_POINT_KEY);
            activePage = savedInstanceState.getInt(ACTIVE_PAGE);
        } else {
            activePage = 0;
        }
        ruler = new RulerDrawable(getActivity(), 0, 7000, initialValue, 0.2f, 400f, 4f);
        rulerImage = (ImageView) rootView.findViewById(R.id.ruler_image);
        rulerImage.setImageDrawable(ruler);
        motionController = new MotionController();
        rulerContainer = rootView.findViewById(R.id.ruler_conteiner);
        rulerContainer.setOnTouchListener(motionController);
        seeker = new Seeker(getActivity(), (ImageView)rootView.findViewById(R.id.cal_value), String.valueOf(initialValue));
        ruler.addOnValueChangedListener(seeker);

        pages = new ArrayList<>();
        ViewGroup pager = (ViewGroup)rootView.findViewById(R.id.pager);
        pager.getChildCount();
        for (int i = 0; i < pager.getChildCount(); i++) {
            int position;
            if (i < activePage) {
                position = Page.BACK;
            } else if (i == activePage) {
                position = Page.MIDDLE;
            } else {
                position = Page.FRONT;
            }
            pages.add(new Page(pager.getChildAt(i), position));
        }

        ImageView progress = (ImageView) rootView.findViewById(R.id.progress);
        progressDrawable = new ProgressDrawable(getActivity());
        progress.setImageDrawable(progressDrawable);


        View continueButton = rootView.findViewById(R.id.continue_button);
        continueButton.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(RULER_MIDDLE_POINT_KEY, ruler.getCurrentMiddleValue());
        outState.putInt(ACTIVE_PAGE, activePage);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        if (activePage < pages.size()-1) {
            pages.get(activePage++).setPosition(Page.BACK);
            pages.get(activePage).setPosition(Page.MIDDLE);
            progressDrawable.stepForward();
        }

//        final Animator anim = ObjectAnimator.ofFloat(this, PROGRESS, fromValue, toValue);
//        anim.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
////                    mIsPlay = !mIsPlay;
//            }
//        });



    }

    public void pageBack() {
        if (activePage > 0) {
            pages.get(activePage--).setPosition(Page.FRONT);
            pages.get(activePage).setPosition(Page.MIDDLE);
            progressDrawable.stepBackward();
        }
    }

    private class MotionController implements View.OnTouchListener{
        private float mPreviousX;
        private float latestSpeed;
        private boolean isPressed = false;

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            float x = event.getX();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    isPressed = true;
                    ruler.pinDown();
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (isPressed) {
//                        seeker.adjustX(x - mPreviousX);
                        latestSpeed = x - mPreviousX;
                        ruler.setSeekerPosition(latestSpeed);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    isPressed = false;
                    ruler.stopScrolling(latestSpeed);
                    break;
            }
            mPreviousX = x;
            return true;
        }
    }
}
