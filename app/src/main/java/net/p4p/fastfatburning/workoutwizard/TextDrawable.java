package net.p4p.fastfatburning.workoutwizard;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import net.p4p.fastfatburning.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by timur on 6/16/16.
 */
public class TextDrawable extends Drawable {
    private static final String TAG = TextDrawable.class.getName();

    private static final float PROGRESS_STEP = 0.01f;
    Paint paint;
    Activity context;
    List<Curtain> curtains;
    ScheduledExecutorService scheduler = null;


    public enum CurtainDirection {
        LEFT, RIGHT
    }

    private class Curtain {
        CurtainDirection direction;
        String text;
        float rightPadding;
        float leftPadding;

        public Curtain(String text, CurtainDirection direction) {
            this.text = text;
            this.direction = direction;
            if (direction == CurtainDirection.LEFT) {
                rightPadding = 0f;
                leftPadding = 1.0f;
            } else {
                rightPadding = 1.0f;
                leftPadding = 0f;
            }
        }

        public Curtain(String text) {
            this.text = text;
            direction = CurtainDirection.LEFT;
            rightPadding = 0f;
            leftPadding = 0f;
        }

        public void increaseProgress(float progressLeft, float progressRight) {
            if (direction == CurtainDirection.RIGHT) {
                if (rightPadding - PROGRESS_STEP > progressRight) {
                    rightPadding -= PROGRESS_STEP;
                } else {
                    rightPadding = progressRight;
                }
                leftPadding = progressLeft;
            } else {
                if (leftPadding - PROGRESS_STEP > progressLeft) {
                    leftPadding -= PROGRESS_STEP;
                } else {
                    leftPadding = progressLeft;
                }
                rightPadding = progressRight;
            }
        }
    }

    private class CurtainAnimator implements Runnable {

        @Override
        public void run() {
//            Log.e(TAG, "ITERATION");
            if (curtains.size() > 1) {
                float progressLeft = 0;
                float progressRight = 0;
                int deletionSentinel = curtains.size();
                for (int i = 0; i < curtains.size(); i++) {
                    Curtain curtain = curtains.get(i);
//                    Log.e(TAG, "PROCESSING: " + curtain.text + ", " + curtain.direction);
                    if (progressLeft + progressRight < 1.0) {
                        curtain.increaseProgress(progressLeft, progressRight);

                        if (curtain.direction == CurtainDirection.RIGHT) {
                            progressLeft = 1 - curtain.rightPadding;
                        } else {
                            progressRight = 1 - curtain.leftPadding;
                        }
                    } else {
                        deletionSentinel = i;
                        break;
                    }
                }
//                Log.e(TAG, "DELETION INDEX: " + deletionSentinel);
                if (deletionSentinel < curtains.size()) {
//                    Log.e(TAG, "DELETING");
                    List<Curtain> toDelete = curtains.subList(deletionSentinel, curtains.size());
//                    Log.e(TAG, "DELETE SIZE: " + toDelete.size());
                    for (Curtain curtain : toDelete) {
                        synchronized (TextDrawable.this) {
                            curtains.remove(curtain);
                        }
                    }
//                    curtains.remove(toDelete);
//                    Log.e(TAG, "AFTER DELETION: " + curtains.size());
                }
                Handler mainHandler = new Handler(context.getMainLooper());
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        invalidateSelf();
                    }
                };
                mainHandler.post(r);
            } else {
                Log.e(TAG, "TIME TO STOP");
                if (scheduler != null) {
                    Log.e(TAG, "SHUTTING DOWN");
                    scheduler.shutdownNow();
                    Log.e(TAG, "DONE");
//                    scheduler = null;
                }
            }
        }
    }

    public TextDrawable(Activity context, String value) {
//        Log.e(TAG, "WIDTH: " + getIntrinsicWidth());
        curtains = new ArrayList<>();
        synchronized (this) {
            curtains.add(0, new Curtain(value));
        }
        this.context = context;
        setPaint();
    }

    private void setPaint() {
        paint = new Paint();
        paint.setColor(ContextCompat.getColor(context, R.color.colorWhite));
        paint.setStrokeWidth(5f);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setTextSize(50);
        paint.setAntiAlias(true);
    }

    public void setText(String value, CurtainDirection direction) {
        synchronized (this) {
            curtains.add(0, new Curtain(value, direction));
        }

        Log.e(TAG, "SETTING TEXT");
        if (scheduler == null || (scheduler.isShutdown() && scheduler.isTerminated())) {
            Log.e(TAG, "NEW EXECUTOR");
            scheduler =
                    Executors.newSingleThreadScheduledExecutor();
            scheduler.scheduleAtFixedRate
                    (new CurtainAnimator(), 0, 5, TimeUnit.MILLISECONDS);
            Log.e(TAG, "DONE");
        }
    }

    @Override
    public void draw(Canvas canvas) {
//        canvas.drawText("tst", 80, 80, paint);

//        Path path = new Path();
//        path.moveTo(100, 40);
//        path.lineTo(100, 100);
//        path.lineTo(120, 100);
//        path.lineTo(120, 40);
//        canvas.clipPath(path);

//        canvas.drawARGB(100, 124, 124, 124);

        float width = canvas.getWidth();
        synchronized (this) {
            for (Curtain curtain : curtains) {
                Bitmap b = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(b);

                float leftBorder = curtain.leftPadding * width;
                float rightBorder = width - curtain.rightPadding * width;
                Path path = new Path();
                path.moveTo(leftBorder, 0);
                path.lineTo(leftBorder, canvas.getHeight());
                path.lineTo(rightBorder, canvas.getHeight());
                path.lineTo(rightBorder, 0);

                c.clipPath(path);
                c.drawText(curtain.text, 10, canvas.getHeight() - 10, paint);
                canvas.drawBitmap(b, 0, 0, paint);
            }
        }
//
//        Bitmap b2 = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
//        Canvas c2 = new Canvas(b2);
//        path = new Path();
//        path.moveTo(50, 0);
//        path.lineTo(50, canvas.getHeight());
//        path.lineTo(canvas.getWidth(), canvas.getHeight());
//        path.lineTo(canvas.getWidth(), 0);
//        c2.clipPath(path);
//        c2.drawText("000", 10, canvas.getHeight() - 10, paint);
//        canvas.drawBitmap(b2, 0, 0, paint);
    }


    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }
}
