package net.p4p.fastfatburning;

import android.app.Activity;
import android.util.DisplayMetrics;

import net.p4p.fastfatburning.workoutwizard.RulerDrawable;
import net.p4p.fastfatburning.workoutwizard.Seeker;

class Choreographer {
    private Seeker seeker;
    private RulerDrawable ruler;
    private float initialPosition;

    private float lastSeekerPosition;
//    private RulerDrawable.Point selectedRulerNumber;

//    private final Property<Choreographer, Float> PROGRESS =
//            new Property<Choreographer, Float>(Float.class, "PROGRESS") {
//                @Override
//                public Float get(Choreographer d) {
//                    return d.getProgress();
//                }
//
//                @Override
//                public void set(Choreographer d, Float value) {
//                    d.setProgress(value);
//                }
//            };

//    public float getProgress() {
//        return seeker.getPosition();
//    }

    public void setProgress(float value) {
//        view.setX(position - (view.getWidth() / 2f));
        float diff = (lastSeekerPosition - initialPosition) * value;
//        seeker.setPosition(initialPosition + diff);
//        diff = (selectedRulerNumber.getCoord() - initialPosition) * value;
//        ruler.setPositionToNumber(selectedRulerNumber.getValue(), initialPosition + diff);
    }

    public Choreographer(Activity context, Seeker seeker, RulerDrawable ruler) {
        this.seeker = seeker;
        this.ruler = ruler;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        initialPosition = (width / 2);
    }

    public void animateSeekerReturn() {
//        ruler.stopScrolling();

//        lastSeekerPosition = seeker.getPosition();
//        selectedRulerNumber = ruler.getSeekerClosestNumber();
//        final Animator anim = ObjectAnimator.ofFloat(this, PROGRESS, 1f, 0f);
//        anim.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                    mIsPlay = !mIsPlay;
//            }
//        });
//        anim.setInterpolator(new DecelerateInterpolator(3));
//        anim.setDuration(600);
//        anim.start();
    }
}