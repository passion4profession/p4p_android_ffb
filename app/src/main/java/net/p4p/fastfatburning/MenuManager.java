package net.p4p.fastfatburning;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.p4p.fastfatburning.workoutwizard.WorkoutComposerFragment;

/**
 * Created by timur on 8/12/16.
 */
public class MenuManager {
    private static final String TAG = MenuManager.class.getName();


    enum MenuItemType {
        HEADER, PRIMARY, SECONDARY, SPECIAL
    }

    static class MenuItem {
        MenuItemType type;

        public MenuItem(MenuItemType type, String title, final Class clazz, ViewGroup root, final Activity context) {
            this.type = type;
            LayoutInflater inflater = context.getLayoutInflater();
            View view;
            switch (type) {
                case HEADER:
                    view = inflater.inflate(R.layout.menu_header, root, false);
                    break;
                case PRIMARY:
                    view = inflater.inflate(R.layout.menu_primary_item, root, false);
                    break;
                case SECONDARY:
                    view = inflater.inflate(R.layout.menu_secondary_item, root, false);
                    break;
                default:
                    view = inflater.inflate(R.layout.menu_secondary_item, root, false);
            }

            TextView text = (TextView) view.findViewById(R.id.title);
            if (text != null) {
                text.setText(title);
            }

            if (type != MenuItemType.SPECIAL) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (clazz != null) {
                            if (Fragment.class.isAssignableFrom(clazz)) {
                                Fragment fragment = null;
                                try {
                                    FragmentManager fragmentManager = context.getFragmentManager();
                                    fragment = (Fragment) clazz.newInstance();
                                    fragmentManager.beginTransaction()
                                            .replace(R.id.content_container, fragment)
                                            .commitAllowingStateLoss();
                                } catch (InstantiationException e) {
                                    e.printStackTrace();
                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        DrawerLayout drawer = (DrawerLayout) context.findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                    }
                });
            }

            root.addView(view);

        }
    }

    static public void fillMenu(ViewGroup root, Activity activity) {
        ViewGroup.LayoutParams lp = root.getLayoutParams();
        lp.width = 600;
        root.setLayoutParams(lp);
        new MenuItem(MenuItemType.HEADER, "welcome", ProfileFragment.class, root, activity);
        new MenuItem(MenuItemType.PRIMARY, "Workout", WorkoutComposerFragment.class, root, activity);
        new MenuItem(MenuItemType.PRIMARY, "Settings", SettingsFragment.class, root, activity);
        new MenuItem(MenuItemType.SECONDARY, "Item", null, root, activity);
    }
}
